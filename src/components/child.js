function Child(props) {
    function handleChangeChild(event) {
        // Here, we invoke the callback with the new value
        props.onChange(event.target.value);
    }
  
    return <input value={props.value} onChange={handleChangeChild} />
}
export default Child;