import { useState } from "react";
import { Container, Input, Nav, NavItem, NavLink, Row, Col, Button } from "reactstrap";
import LoginForm from "./login-form";

function SignUpForm(props) {
    const SignUpClick = () => {
        props.onChange(false)
    }
    const LoginClick = () => {
        props.onChange(true)
    }
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const inputFirstName = (paramFirstName) => {
        setFirstName(paramFirstName.target.value);
    }
    const inputLastName = (paramLastName) => {
        setLastName(paramLastName.target.value);
    }

    const inputEmail = (paramEmail) => {
        setEmail(paramEmail.target.value);
    }
    const inputPassword = (paramPassword) => {
        setPassword(paramPassword.target.value);
    }
    const [validateEmail, setValidateEmail] = useState(false);
    const [validatePassword, setValidatPasword] = useState(false);
    const onGetStartClick = () => {
        var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        var checkEmail = re.test(email.trim());
        if (checkEmail && password==='123456') {
            setValidateEmail(true);
            setValidatPasword(true);
            console.log("Thông tin hợp lệ");
            console.log("Email: " + email);
            console.log("Password: " + password);
        }
        else {
            setValidateEmail(false);
            setValidatPasword(false);
            console.log("Thông tin không hợp lệ");
            console.log("Email: " + email);
            console.log("Password: " + password);
        }
    }
    return (
        <div className="form">
            <Container>
                <div>
                    <Nav
                    >
                        <NavItem>
                            <NavLink
                                onClick={SignUpClick}
                                active
                                href="/#"
                            >
                                Sign Up
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                onClick={LoginClick}
                                href={LoginForm}
                            >
                                Login
                            </NavLink>
                        </NavItem>
                    </Nav>
                </div>
                <p className="text-center text-white">Sign up for free!</p>
                <Row>
                    <Col sm='6'>
                        <Input placeholder="First name*" value={firstName} onChange={inputFirstName} />
                    </Col>
                    <Col sm='6'>
                        <Input placeholder="Last name*" value={lastName} onChange={inputLastName} />
                    </Col>
                </Row>
                <Row>
                    <Col sm='12'>
                        <Input placeholder="Email Address*" value={email} onChange={inputEmail} />
                    </Col>
                </Row>
                <Row>
                    <Col sm='12'>
                        <Input placeholder="Set A Password*" value={password} onChange={inputPassword} />
                    </Col>
                </Row>
                <Row>
                    <Col sm='12'>
                        <Button onClick={onGetStartClick}>GET STARTED</Button>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default SignUpForm;