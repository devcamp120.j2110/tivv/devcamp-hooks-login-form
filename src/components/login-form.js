import { useState } from "react";
import { Container, Input, Nav, NavItem, NavLink, Row, Col, Button } from "reactstrap";
import SignUpForm from "./signup-form";

function LoginForm(props) {
    const SignUpClick = () => {
        props.onChange(false);
    }
    const LoginClick = () => {
        props.onChange(true);
    }
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const inputEmail = (paramEmail) => {
        setEmail(paramEmail.target.value);
    }
    const inputPassword = (paramPassword) => {
        setPassword(paramPassword.target.value);
    }

    const [validateEmail, setValidateEmail] = useState(false);
    const [validatePassword, setValidatPasword] = useState(false);
    const onGetStartClick = () => {
        var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        var checkEmail = re.test(email.trim());
        if (checkEmail && password==='123456') {
            setValidateEmail(true);
            setValidatPasword(true);
            console.log("Thông tin hợp lệ");
            console.log("Email: " + email);
            console.log("Password: " + password);
        }
        else {
            setValidateEmail(false);
            setValidatPasword(false);
            console.log("Thông tin không hợp lệ");
            console.log("Email: " + email);
            console.log("Password: " + password);
        }
    }
    return (
        <div className="form">
            <Container>
                <div>
                    <Nav
                    >
                        <NavItem>
                            <NavLink
                                onClick={SignUpClick}
                                href={SignUpForm}
                            >
                                Sign Up
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                onClick={LoginClick}
                                active
                                href="/#"
                            >
                                Login
                            </NavLink>
                        </NavItem>
                    </Nav>
                </div>
                <p className="text-center text-white">Welcome Back!</p>
                <Row>
                    <Col sm='12'>
                        <Input placeholder="Email Address*" value={email} onChange={inputEmail} />
                    </Col>
                </Row>
                <Row>
                    <Col sm='12'>
                        <Input placeholder="Set A Password*" value={password} onChange={inputPassword} />
                    </Col>
                </Row>
                <a href="/#">Forget Password?</a>
                <Row>
                    <Col sm='12'>
                        <Button onClick={onGetStartClick}>GET STARTED</Button>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default LoginForm;