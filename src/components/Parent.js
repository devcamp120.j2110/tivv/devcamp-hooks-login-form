import {useState} from 'react'
import Child from './child';
function Parent() {
    const [value, setValue] = useState("")

    function handleChange(newValue) {
      setValue(newValue);
    }

    // We pass a callback to Child
    return <Child value={value} onChange={handleChange} />;
}
export default Parent;