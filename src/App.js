import 'bootstrap/dist/css/bootstrap.min.css'
import LoginForm from './components/login-form';
import './App.css'
import { useState } from 'react';
import SignUpForm from './components/signup-form';


function App() {
const [display, setDisplay] = useState(true);
const onChangeHandle = (value) => {
  setDisplay(value);
}
  return (
    <div>
      {display ? <LoginForm onChange={onChangeHandle}/> : <SignUpForm onChange={onChangeHandle}/>}
    </div>
  );
}

export default App;
